#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/28 9:59
# @Author  : Link
# @Site    : 
# @File    : vue_interface.py
# @Software: PyCharm
from PyQt5.QtCore import QObject, pyqtSignal

from .channel_interface import VueElementObject


# 功能挂载
class MainInterFace(VueElementObject, QObject):
    """
    append_status_message: 用于在qt statusbar message列表中添加信息
    set_status_message_style: 用于设置 qt statusbar 的style 还未编写
    message: 用于QT显示QMessageBox
    """
    append_status_message = pyqtSignal(str)
    set_status_message_style = pyqtSignal(str)
    message = pyqtSignal(str)

    def __init__(self):
        super(MainInterFace, self).__init__()
