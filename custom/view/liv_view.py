#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/23 13:04
# @Author  : Link
# @Site    : 
# @File    : liv_view.py
# @Software: PyCharm

"""
在后台出现的重大问题都需要qt app来给前端进行提示! WEB不值得相信
"""
from custom.fake_data.charts import generator_echarts_data
from custom.fake_data.table import generator_table
from frame import ViewInterface
from frame import request_vue_data, return_vue_data
from PyQt5.QtCore import QTimer


class TO_LIV_KEY(ViewInterface):
    # 小写用于qt
    app = None
    liv_parameter = None  # 获取的测试档信息
    # 大写用于前端
    APP_OPERATE_CONTROL = True  # 前端的操作权限

    SELECT_GROUP = {
        "APP_SELECT_UP_LIST": [],  # 1-10
        "APP_SELECT_DOWN_LIST": []  # 11-12
    }
    APP_TEXTAREA = ""
    APP_NOW_STATION = 0  # 当前测试位置号

    APP_RESULT_DATA = {
        "DATA": None,
        "HEAD": None,
    }  # 返回给前端table的测试数据汇总

    APP_FORM = {
        "APP_MANUFACTURE_ID": "",
        "APP_PARAMETER_ID": None,
        "APP_BOARD_NO": 0,
        "APP_JUDGING": 0,
        "APP_AUTO_BOARD": False,  # 板号自动加一
    }

    APP_CHART_DATA = None  # ECharts

    def __init__(self, vue_obj):
        """
        :param app: QMainWindow
        :param vue_obj: VueElementObject 携带 pyqtSignal
        """
        super(TO_LIV_KEY, self).__init__(vue_obj=vue_obj)

    @request_vue_data
    def liv_start(self, APP_FORM: dict):
        """
        @request_vue_data 装饰器 接收从前端传来的数据 前端用法 this.$request('get_app_status');
        :return:
        """
        self.__set_control_status(False)
        self.set_fail_message("测试 异常信息响应")
        self.set_other_message(str(APP_FORM), 201)
        QTimer.singleShot(2000, lambda: self.__set_control_status(True))
        QTimer.singleShot(3000, lambda: self.__set_echarts_data(generator_echarts_data()))
        QTimer.singleShot(4000, lambda: self.__set_table_data({
            "DATA": generator_table(10),
            "HEAD": ['id', 'title', 'author', 'display_time']
        }))

    @request_vue_data
    def checkbox_change(self, SELECT_GROUP: dict):
        """
        前端用法 this.$request('checkbox_change', this.SELECT_GROUP);
        :param kwargs: this.SELECT_GROUP
        :return:
        """
        self.vue_obj.append_status_message.emit("CHECKBOX 更新成功!")  # QT
        self.__set_checkbox_status(SELECT_GROUP)

    @request_vue_data
    def get_app_status(self):
        """
        顺便把其他的状态也给前端
        :return:
        """
        self.__checkbox_emit()
        self.__control_emit()
        self.__message_emit()
        self.__table_data_emit()
        self.__echarts_data_emit()

    # 带下划线(单双均可 双下划线方法改名后也会带单下划线) __的函数 不会被注册 参考ViewInterface类
    # 写的有点多了 反正下面的写在一起也无所谓 主要是@return_vue_data装饰的函数 要在前端进行绑定
    # 带下划线无装饰器: 内部函数
    def __set_control_status(self, APP_OPERATE_CONTROL: bool):
        self.APP_OPERATE_CONTROL = APP_OPERATE_CONTROL
        self.__control_emit()

    def __set_checkbox_status(self, SELECT_GROUP: dict):
        self.SELECT_GROUP = SELECT_GROUP
        self.__checkbox_emit()

    def __set_message_status(self, APP_TEXTAREA: str):
        self.APP_TEXTAREA = APP_TEXTAREA
        self.__message_emit()

    def __set_table_data(self, APP_RESULT_DATA: dict):
        self.APP_RESULT_DATA = APP_RESULT_DATA
        self.__table_data_emit()

    def __set_echarts_data(self, APP_CHART_DATA: dict):
        self.APP_CHART_DATA = APP_CHART_DATA
        self.__echarts_data_emit()

    # 带下划线有装饰器的代表是只从QT端往前端发送
    """
      this.$QtCallBackDict.__control_emit = this.SetAppOperateControl;
      this.$QtCallBackDict.__checkbox_emit = this.SetCheckBoxStates;
      this.$QtCallBackDict.__message_emit = this.SetMessageStates;
    """

    @return_vue_data
    def __checkbox_emit(self):
        return self.SELECT_GROUP

    @return_vue_data
    def __control_emit(self):
        return self.APP_OPERATE_CONTROL

    @return_vue_data
    def __message_emit(self):
        return self.APP_TEXTAREA

    @return_vue_data
    def __table_data_emit(self):
        return self.APP_RESULT_DATA

    @return_vue_data
    def __echarts_data_emit(self):
        return self.APP_CHART_DATA
