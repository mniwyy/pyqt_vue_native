#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/28 10:08
# @Author  : Link
# @Site    : 
# @File    : main_ui.py
# @Software: PyCharm

from functools import partial  # https://blog.csdn.net/qq_33688922/article/details/91890142
# 用于解决通过循环将控件绑定信号
from PyQt5 import uic, QtGui
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QTabWidget, QSlider, QPushButton, QCheckBox, QDoubleSpinBox, \
    QAbstractSpinBox, QProgressBar
from PyQt5.QtCore import Qt
import datetime as dt
import frame.ui.pyqtsource_rc

from frame.component.led_indicator import LedIndicator


class Main_Ui(QMainWindow):
    def __init__(self, parent=None):
        super(Main_Ui, self).__init__(parent)
        uic.loadUi('frame/ui/gui.ui', self)

        self.table = QTabWidget(self)
        self.table.setTabsClosable(True)
        self.table.tabCloseRequested.connect(self.close_table_signal)

        # 添加缩小按钮
        self.pushButton = QPushButton("", self)
        self.pushButton.setShortcut("Ctrl+-")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/pyqt/source/images/lc_zoomout.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton.setIcon(icon)
        # 添加放大按钮
        self.pushButton_2 = QPushButton("", self)
        self.pushButton_2.setShortcut("Ctrl++")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/pyqt/source/images/lc_zoomin.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_2.setIcon(icon1)

        # 添加水平条
        self.horizontalSlider = QSlider(self)
        self.horizontalSlider.setMaximumWidth(150)
        self.horizontalSlider.setMaximum(500)
        self.horizontalSlider.setMinimum(25)
        self.horizontalSlider.setPageStep(10)
        self.horizontalSlider.setValue(100)
        self.horizontalSlider.setOrientation(Qt.Horizontal)

        # 指示当前的zoom
        self.zoom_double_spinbox = QDoubleSpinBox(self)
        self.zoom_double_spinbox.setMaximum(1000.0)
        self.zoom_double_spinbox.setSuffix("%")
        self.zoom_double_spinbox.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.zoom_double_spinbox.setValue(100)

        # 添加状态栏 process_bar
        self.tool_process_bar = QProgressBar(self)
        self.tool_process_bar.setTextVisible(False)
        self.tool_process_bar.setMaximumWidth(200)

        # 添加checkbox
        self.view_last_table = QCheckBox("跳转到新建页面", self)

        # 添加信号 # pyqtSlot 又没法用了
        self.pushButton.pressed.connect(self._on_pushButton_pressed)
        self.pushButton_2.pressed.connect(self._on_pushButton_2_pressed)

        # 添加到状态栏
        self.statusbar.addPermanentWidget(self.tool_process_bar)

        self.statusbar.addPermanentWidget(self.pushButton)
        self.statusbar.addPermanentWidget(self.horizontalSlider)
        self.statusbar.addPermanentWidget(self.pushButton_2)
        self.statusbar.addPermanentWidget(self.zoom_double_spinbox)
        self.statusbar.addPermanentWidget(self.view_last_table)
        self.led_device = LedIndicator()
        self.statusbar.addPermanentWidget(self.led_device)

        self.view_tables = []
        self.view_tables_ids = []
        self.error_message = []
        self.gridLayout.addWidget(self.table)

    # @pyqtSlot()
    def _on_pushButton_pressed(self):
        value = self.horizontalSlider.value() - 10
        self.horizontalSlider.setValue(value)
        self.zoom_double_spinbox.setValue(value)

    def _on_pushButton_2_pressed(self):
        value = self.horizontalSlider.value() + 10
        self.horizontalSlider.setValue(value)
        self.zoom_double_spinbox.setValue(value)

    def close_table_signal(self, index: int):
        if index == 0:
            return self.message_show("无法删除主窗口")
        self.table.removeTab(index)
        self.view_tables.pop(index)
        self.view_tables_ids.pop(index)

    def append_message(self, text: str):
        if len(self.error_message) > 3:
            self.error_message.pop()
        self.error_message.insert(0,
                                  "< INFO: {} > < DATETIME:{} >".format(text, dt.datetime.now().strftime("%H:%M:%S")))
        self.show_message()

    def show_message(self):
        self.statusbar.showMessage(' || '.join(self.error_message))

    def message_show(self, text):
        res = QMessageBox.question(self, '请确认', text,
                                   QMessageBox.Yes | QMessageBox.No,
                                   QMessageBox.Yes)
        if res == QMessageBox.Yes:
            return True
        else:
            return False
