#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : table.py
@Author  : Link
@Time    : 2021/4/17 13:46
"""
from custom.fake_data.table import generator_table
from frame import VueElementObject
from frame import returnSuccess


class A:
    def __init__(self, vue_obj: VueElementObject):
        vue_obj.func_dict["getTableData"] = self.get_table_data

    def get_table_data(self, req: dict):
        if req["func"] != 'getTableData':
            raise Exception("func error")
        return returnSuccess("success", func=req["func"], data=generator_table(req["data"]["row"]))
